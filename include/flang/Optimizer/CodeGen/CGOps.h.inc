/*===- TableGen'erated file -------------------------------------*- C++ -*-===*\
|*                                                                            *|
|* Op Declarations                                                            *|
|*                                                                            *|
|* Automatically generated file, do not edit!                                 *|
|*                                                                            *|
\*===----------------------------------------------------------------------===*/

#if defined(GET_OP_CLASSES) || defined(GET_OP_FWD_DEFINES)
#undef GET_OP_FWD_DEFINES
namespace fir {
namespace cg {
class XArrayCoorOp;
} // namespace cg
} // namespace fir
namespace fir {
namespace cg {
class XEmboxOp;
} // namespace cg
} // namespace fir
namespace fir {
namespace cg {
class XReboxOp;
} // namespace cg
} // namespace fir
#endif

#ifdef GET_OP_CLASSES
#undef GET_OP_CLASSES


//===----------------------------------------------------------------------===//
// Local Utility Method Definitions
//===----------------------------------------------------------------------===//

namespace fir {
namespace cg {

//===----------------------------------------------------------------------===//
// ::fir::cg::XArrayCoorOp declarations
//===----------------------------------------------------------------------===//

class XArrayCoorOpAdaptor {
public:
  XArrayCoorOpAdaptor(::mlir::ValueRange values, ::mlir::DictionaryAttr attrs = nullptr, ::mlir::RegionRange regions = {});

  XArrayCoorOpAdaptor(XArrayCoorOp &op);

  ::mlir::ValueRange getOperands();
  std::pair<unsigned, unsigned> getODSOperandIndexAndLength(unsigned index);
  ::mlir::ValueRange getODSOperands(unsigned index);
  ::mlir::Value memref();
  ::mlir::ValueRange shape();
  ::mlir::ValueRange shift();
  ::mlir::ValueRange slice();
  ::mlir::ValueRange subcomponent();
  ::mlir::ValueRange indices();
  ::mlir::ValueRange lenParams();
  ::mlir::DictionaryAttr getAttributes();
  ::mlir::LogicalResult verify(::mlir::Location loc);
private:
  ::mlir::ValueRange odsOperands;
  ::mlir::DictionaryAttr odsAttrs;
  ::mlir::RegionRange odsRegions;
};
class XArrayCoorOp : public ::mlir::Op<XArrayCoorOp, ::mlir::OpTrait::ZeroRegion, ::mlir::OpTrait::OneResult, ::mlir::OpTrait::OneTypedResult<::fir::ReferenceType>::Impl, ::mlir::OpTrait::ZeroSuccessor, ::mlir::OpTrait::AtLeastNOperands<1>::Impl, ::mlir::OpTrait::AttrSizedOperandSegments> {
public:
  using Op::Op;
  using Op::print;
  using Adaptor = XArrayCoorOpAdaptor;
  static ::llvm::ArrayRef<::llvm::StringRef> getAttributeNames() {
    static ::llvm::StringRef attrNames[] = {::llvm::StringRef("operand_segment_sizes")};
    return ::llvm::makeArrayRef(attrNames);
  }

  ::mlir::StringAttr operand_segment_sizesAttrName() {
    return getAttributeNameForIndex(0);
  }

  static ::mlir::StringAttr operand_segment_sizesAttrName(::mlir::OperationName name) {
    return getAttributeNameForIndex(name, 0);
  }

  static constexpr ::llvm::StringLiteral getOperationName() {
    return ::llvm::StringLiteral("fircg.ext_array_coor");
  }

  std::pair<unsigned, unsigned> getODSOperandIndexAndLength(unsigned index);
  ::mlir::Operation::operand_range getODSOperands(unsigned index);
  ::mlir::Value memref();
  ::mlir::Operation::operand_range shape();
  ::mlir::Operation::operand_range shift();
  ::mlir::Operation::operand_range slice();
  ::mlir::Operation::operand_range subcomponent();
  ::mlir::Operation::operand_range indices();
  ::mlir::Operation::operand_range lenParams();
  ::mlir::MutableOperandRange memrefMutable();
  ::mlir::MutableOperandRange shapeMutable();
  ::mlir::MutableOperandRange shiftMutable();
  ::mlir::MutableOperandRange sliceMutable();
  ::mlir::MutableOperandRange subcomponentMutable();
  ::mlir::MutableOperandRange indicesMutable();
  ::mlir::MutableOperandRange lenParamsMutable();
  std::pair<unsigned, unsigned> getODSResultIndexAndLength(unsigned index);
  ::mlir::Operation::result_range getODSResults(unsigned index);
  static void build(::mlir::OpBuilder &odsBuilder, ::mlir::OperationState &odsState, ::mlir::Type resultType0, ::mlir::Value memref, ::mlir::ValueRange shape, ::mlir::ValueRange shift, ::mlir::ValueRange slice, ::mlir::ValueRange subcomponent, ::mlir::ValueRange indices, ::mlir::ValueRange lenParams);
  static void build(::mlir::OpBuilder &odsBuilder, ::mlir::OperationState &odsState, ::mlir::TypeRange resultTypes, ::mlir::Value memref, ::mlir::ValueRange shape, ::mlir::ValueRange shift, ::mlir::ValueRange slice, ::mlir::ValueRange subcomponent, ::mlir::ValueRange indices, ::mlir::ValueRange lenParams);
  static void build(::mlir::OpBuilder &, ::mlir::OperationState &odsState, ::mlir::TypeRange resultTypes, ::mlir::ValueRange operands, ::llvm::ArrayRef<::mlir::NamedAttribute> attributes = {});
  ::mlir::LogicalResult verify();
  static ::mlir::ParseResult parse(::mlir::OpAsmParser &parser, ::mlir::OperationState &result);
  void print(::mlir::OpAsmPrinter &_odsPrinter);
private:
  ::mlir::StringAttr getAttributeNameForIndex(unsigned index) {
    return getAttributeNameForIndex((*this)->getName(), index);
  }

  static ::mlir::StringAttr getAttributeNameForIndex(::mlir::OperationName name, unsigned index) {
    assert(index < 1 && "invalid attribute index");
    return name.getRegisteredInfo()->getAttributeNames()[index];
  }

public:
  unsigned getRank();

  // Shape is optional, but if it exists, it will be at offset 1.
  unsigned shapeOffset() { return 1; }
  unsigned shiftOffset() { return shapeOffset() + shape().size(); }
  unsigned sliceOffset() { return shiftOffset() + shift().size(); }
  unsigned subcomponentOffset() { return sliceOffset() + slice().size(); }
  unsigned indicesOffset() {
    return subcomponentOffset() + subcomponent().size();
  }
  unsigned lenParamsOffset() { return indicesOffset() + indices().size(); }
};
} // namespace cg
} // namespace fir
DECLARE_EXPLICIT_TYPE_ID(::fir::cg::XArrayCoorOp)

namespace fir {
namespace cg {

//===----------------------------------------------------------------------===//
// ::fir::cg::XEmboxOp declarations
//===----------------------------------------------------------------------===//

class XEmboxOpAdaptor {
public:
  XEmboxOpAdaptor(::mlir::ValueRange values, ::mlir::DictionaryAttr attrs = nullptr, ::mlir::RegionRange regions = {});

  XEmboxOpAdaptor(XEmboxOp &op);

  ::mlir::ValueRange getOperands();
  std::pair<unsigned, unsigned> getODSOperandIndexAndLength(unsigned index);
  ::mlir::ValueRange getODSOperands(unsigned index);
  ::mlir::Value memref();
  ::mlir::ValueRange shape();
  ::mlir::ValueRange shift();
  ::mlir::ValueRange slice();
  ::mlir::ValueRange subcomponent();
  ::mlir::ValueRange substr();
  ::mlir::ValueRange lenParams();
  ::mlir::DictionaryAttr getAttributes();
  ::mlir::LogicalResult verify(::mlir::Location loc);
private:
  ::mlir::ValueRange odsOperands;
  ::mlir::DictionaryAttr odsAttrs;
  ::mlir::RegionRange odsRegions;
};
class XEmboxOp : public ::mlir::Op<XEmboxOp, ::mlir::OpTrait::ZeroRegion, ::mlir::OpTrait::OneResult, ::mlir::OpTrait::OneTypedResult<::fir::BoxType>::Impl, ::mlir::OpTrait::ZeroSuccessor, ::mlir::OpTrait::AtLeastNOperands<1>::Impl, ::mlir::OpTrait::AttrSizedOperandSegments> {
public:
  using Op::Op;
  using Op::print;
  using Adaptor = XEmboxOpAdaptor;
  static ::llvm::ArrayRef<::llvm::StringRef> getAttributeNames() {
    static ::llvm::StringRef attrNames[] = {::llvm::StringRef("operand_segment_sizes")};
    return ::llvm::makeArrayRef(attrNames);
  }

  ::mlir::StringAttr operand_segment_sizesAttrName() {
    return getAttributeNameForIndex(0);
  }

  static ::mlir::StringAttr operand_segment_sizesAttrName(::mlir::OperationName name) {
    return getAttributeNameForIndex(name, 0);
  }

  static constexpr ::llvm::StringLiteral getOperationName() {
    return ::llvm::StringLiteral("fircg.ext_embox");
  }

  std::pair<unsigned, unsigned> getODSOperandIndexAndLength(unsigned index);
  ::mlir::Operation::operand_range getODSOperands(unsigned index);
  ::mlir::Value memref();
  ::mlir::Operation::operand_range shape();
  ::mlir::Operation::operand_range shift();
  ::mlir::Operation::operand_range slice();
  ::mlir::Operation::operand_range subcomponent();
  ::mlir::Operation::operand_range substr();
  ::mlir::Operation::operand_range lenParams();
  ::mlir::MutableOperandRange memrefMutable();
  ::mlir::MutableOperandRange shapeMutable();
  ::mlir::MutableOperandRange shiftMutable();
  ::mlir::MutableOperandRange sliceMutable();
  ::mlir::MutableOperandRange subcomponentMutable();
  ::mlir::MutableOperandRange substrMutable();
  ::mlir::MutableOperandRange lenParamsMutable();
  std::pair<unsigned, unsigned> getODSResultIndexAndLength(unsigned index);
  ::mlir::Operation::result_range getODSResults(unsigned index);
  static void build(::mlir::OpBuilder &odsBuilder, ::mlir::OperationState &odsState, ::mlir::Type resultType0, ::mlir::Value memref, ::mlir::ValueRange shape, ::mlir::ValueRange shift, ::mlir::ValueRange slice, ::mlir::ValueRange subcomponent, ::mlir::ValueRange substr, ::mlir::ValueRange lenParams);
  static void build(::mlir::OpBuilder &odsBuilder, ::mlir::OperationState &odsState, ::mlir::TypeRange resultTypes, ::mlir::Value memref, ::mlir::ValueRange shape, ::mlir::ValueRange shift, ::mlir::ValueRange slice, ::mlir::ValueRange subcomponent, ::mlir::ValueRange substr, ::mlir::ValueRange lenParams);
  static void build(::mlir::OpBuilder &, ::mlir::OperationState &odsState, ::mlir::TypeRange resultTypes, ::mlir::ValueRange operands, ::llvm::ArrayRef<::mlir::NamedAttribute> attributes = {});
  ::mlir::LogicalResult verify();
  static ::mlir::ParseResult parse(::mlir::OpAsmParser &parser, ::mlir::OperationState &result);
  void print(::mlir::OpAsmPrinter &_odsPrinter);
private:
  ::mlir::StringAttr getAttributeNameForIndex(unsigned index) {
    return getAttributeNameForIndex((*this)->getName(), index);
  }

  static ::mlir::StringAttr getAttributeNameForIndex(::mlir::OperationName name, unsigned index) {
    assert(index < 1 && "invalid attribute index");
    return name.getRegisteredInfo()->getAttributeNames()[index];
  }

public:
  // The rank of the entity being emboxed
  unsigned getRank() { return shape().size(); }

  // The rank of the result. A slice op can reduce the rank.
  unsigned getOutRank();

  // The shape operands are mandatory and always start at 1.
  unsigned shapeOffset() { return 1; }
  unsigned shiftOffset() { return shapeOffset() + shape().size(); }
  unsigned sliceOffset() { return shiftOffset() + shift().size(); }
  unsigned subcomponentOffset() { return sliceOffset() + slice().size(); }
  unsigned substrOffset() {
    return subcomponentOffset() + subcomponent().size();
  }
  unsigned lenParamOffset() { return substrOffset() + substr().size(); }
};
} // namespace cg
} // namespace fir
DECLARE_EXPLICIT_TYPE_ID(::fir::cg::XEmboxOp)

namespace fir {
namespace cg {

//===----------------------------------------------------------------------===//
// ::fir::cg::XReboxOp declarations
//===----------------------------------------------------------------------===//

class XReboxOpAdaptor {
public:
  XReboxOpAdaptor(::mlir::ValueRange values, ::mlir::DictionaryAttr attrs = nullptr, ::mlir::RegionRange regions = {});

  XReboxOpAdaptor(XReboxOp &op);

  ::mlir::ValueRange getOperands();
  std::pair<unsigned, unsigned> getODSOperandIndexAndLength(unsigned index);
  ::mlir::ValueRange getODSOperands(unsigned index);
  ::mlir::Value box();
  ::mlir::ValueRange shape();
  ::mlir::ValueRange shift();
  ::mlir::ValueRange slice();
  ::mlir::ValueRange subcomponent();
  ::mlir::ValueRange substr();
  ::mlir::DictionaryAttr getAttributes();
  ::mlir::LogicalResult verify(::mlir::Location loc);
private:
  ::mlir::ValueRange odsOperands;
  ::mlir::DictionaryAttr odsAttrs;
  ::mlir::RegionRange odsRegions;
};
class XReboxOp : public ::mlir::Op<XReboxOp, ::mlir::OpTrait::ZeroRegion, ::mlir::OpTrait::OneResult, ::mlir::OpTrait::OneTypedResult<::fir::BoxType>::Impl, ::mlir::OpTrait::ZeroSuccessor, ::mlir::OpTrait::AtLeastNOperands<1>::Impl, ::mlir::OpTrait::AttrSizedOperandSegments> {
public:
  using Op::Op;
  using Op::print;
  using Adaptor = XReboxOpAdaptor;
  static ::llvm::ArrayRef<::llvm::StringRef> getAttributeNames() {
    static ::llvm::StringRef attrNames[] = {::llvm::StringRef("operand_segment_sizes")};
    return ::llvm::makeArrayRef(attrNames);
  }

  ::mlir::StringAttr operand_segment_sizesAttrName() {
    return getAttributeNameForIndex(0);
  }

  static ::mlir::StringAttr operand_segment_sizesAttrName(::mlir::OperationName name) {
    return getAttributeNameForIndex(name, 0);
  }

  static constexpr ::llvm::StringLiteral getOperationName() {
    return ::llvm::StringLiteral("fircg.ext_rebox");
  }

  std::pair<unsigned, unsigned> getODSOperandIndexAndLength(unsigned index);
  ::mlir::Operation::operand_range getODSOperands(unsigned index);
  ::mlir::Value box();
  ::mlir::Operation::operand_range shape();
  ::mlir::Operation::operand_range shift();
  ::mlir::Operation::operand_range slice();
  ::mlir::Operation::operand_range subcomponent();
  ::mlir::Operation::operand_range substr();
  ::mlir::MutableOperandRange boxMutable();
  ::mlir::MutableOperandRange shapeMutable();
  ::mlir::MutableOperandRange shiftMutable();
  ::mlir::MutableOperandRange sliceMutable();
  ::mlir::MutableOperandRange subcomponentMutable();
  ::mlir::MutableOperandRange substrMutable();
  std::pair<unsigned, unsigned> getODSResultIndexAndLength(unsigned index);
  ::mlir::Operation::result_range getODSResults(unsigned index);
  static void build(::mlir::OpBuilder &odsBuilder, ::mlir::OperationState &odsState, ::mlir::Type resultType0, ::mlir::Value box, ::mlir::ValueRange shape, ::mlir::ValueRange shift, ::mlir::ValueRange slice, ::mlir::ValueRange subcomponent, ::mlir::ValueRange substr);
  static void build(::mlir::OpBuilder &odsBuilder, ::mlir::OperationState &odsState, ::mlir::TypeRange resultTypes, ::mlir::Value box, ::mlir::ValueRange shape, ::mlir::ValueRange shift, ::mlir::ValueRange slice, ::mlir::ValueRange subcomponent, ::mlir::ValueRange substr);
  static void build(::mlir::OpBuilder &, ::mlir::OperationState &odsState, ::mlir::TypeRange resultTypes, ::mlir::ValueRange operands, ::llvm::ArrayRef<::mlir::NamedAttribute> attributes = {});
  ::mlir::LogicalResult verify();
  static ::mlir::ParseResult parse(::mlir::OpAsmParser &parser, ::mlir::OperationState &result);
  void print(::mlir::OpAsmPrinter &_odsPrinter);
private:
  ::mlir::StringAttr getAttributeNameForIndex(unsigned index) {
    return getAttributeNameForIndex((*this)->getName(), index);
  }

  static ::mlir::StringAttr getAttributeNameForIndex(::mlir::OperationName name, unsigned index) {
    assert(index < 1 && "invalid attribute index");
    return name.getRegisteredInfo()->getAttributeNames()[index];
  }

public:
  // The rank of the entity being reboxed
  unsigned getRank();
  // The rank of the result box
  unsigned getOutRank();

  unsigned shapeOffset() { return 1; }
  unsigned shiftOffset() { return shapeOffset() + shape().size(); }
  unsigned sliceOffset() { return shiftOffset() + shift().size(); }
  unsigned subcomponentOffset() { return sliceOffset() + slice().size(); }
  unsigned substrOffset() {
    return subcomponentOffset() + subcomponent().size();
  }
};
} // namespace cg
} // namespace fir
DECLARE_EXPLICIT_TYPE_ID(::fir::cg::XReboxOp)


#endif  // GET_OP_CLASSES

