#----------------------------------------------------------------
# Generated CMake target import file for configuration "Release".
#----------------------------------------------------------------

# Commands may need to know the format version.
set(CMAKE_IMPORT_FILE_VERSION 1)

# Import target "FortranCommon" for configuration "Release"
set_property(TARGET FortranCommon APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(FortranCommon PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELEASE "CXX"
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/lib/libFortranCommon.a"
  )

list(APPEND _IMPORT_CHECK_TARGETS FortranCommon )
list(APPEND _IMPORT_CHECK_FILES_FOR_FortranCommon "${_IMPORT_PREFIX}/lib/libFortranCommon.a" )

# Import target "FortranEvaluate" for configuration "Release"
set_property(TARGET FortranEvaluate APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(FortranEvaluate PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELEASE "CXX"
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/lib/libFortranEvaluate.a"
  )

list(APPEND _IMPORT_CHECK_TARGETS FortranEvaluate )
list(APPEND _IMPORT_CHECK_FILES_FOR_FortranEvaluate "${_IMPORT_PREFIX}/lib/libFortranEvaluate.a" )

# Import target "FortranDecimal" for configuration "Release"
set_property(TARGET FortranDecimal APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(FortranDecimal PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELEASE "CXX"
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/lib/libFortranDecimal.a"
  )

list(APPEND _IMPORT_CHECK_TARGETS FortranDecimal )
list(APPEND _IMPORT_CHECK_FILES_FOR_FortranDecimal "${_IMPORT_PREFIX}/lib/libFortranDecimal.a" )

# Import target "FortranLower" for configuration "Release"
set_property(TARGET FortranLower APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(FortranLower PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELEASE "CXX"
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/lib/libFortranLower.a"
  )

list(APPEND _IMPORT_CHECK_TARGETS FortranLower )
list(APPEND _IMPORT_CHECK_FILES_FOR_FortranLower "${_IMPORT_PREFIX}/lib/libFortranLower.a" )

# Import target "FortranParser" for configuration "Release"
set_property(TARGET FortranParser APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(FortranParser PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELEASE "CXX"
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/lib/libFortranParser.a"
  )

list(APPEND _IMPORT_CHECK_TARGETS FortranParser )
list(APPEND _IMPORT_CHECK_FILES_FOR_FortranParser "${_IMPORT_PREFIX}/lib/libFortranParser.a" )

# Import target "FortranSemantics" for configuration "Release"
set_property(TARGET FortranSemantics APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(FortranSemantics PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELEASE "CXX"
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/lib/libFortranSemantics.a"
  )

list(APPEND _IMPORT_CHECK_TARGETS FortranSemantics )
list(APPEND _IMPORT_CHECK_FILES_FOR_FortranSemantics "${_IMPORT_PREFIX}/lib/libFortranSemantics.a" )

# Import target "flangFrontend" for configuration "Release"
set_property(TARGET flangFrontend APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(flangFrontend PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELEASE "CXX"
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/lib/libflangFrontend.a"
  )

list(APPEND _IMPORT_CHECK_TARGETS flangFrontend )
list(APPEND _IMPORT_CHECK_FILES_FOR_flangFrontend "${_IMPORT_PREFIX}/lib/libflangFrontend.a" )

# Import target "flangFrontendTool" for configuration "Release"
set_property(TARGET flangFrontendTool APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(flangFrontendTool PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELEASE "CXX"
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/lib/libflangFrontendTool.a"
  )

list(APPEND _IMPORT_CHECK_TARGETS flangFrontendTool )
list(APPEND _IMPORT_CHECK_FILES_FOR_flangFrontendTool "${_IMPORT_PREFIX}/lib/libflangFrontendTool.a" )

# Import target "FIRBuilder" for configuration "Release"
set_property(TARGET FIRBuilder APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(FIRBuilder PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELEASE "CXX"
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/lib/libFIRBuilder.a"
  )

list(APPEND _IMPORT_CHECK_TARGETS FIRBuilder )
list(APPEND _IMPORT_CHECK_FILES_FOR_FIRBuilder "${_IMPORT_PREFIX}/lib/libFIRBuilder.a" )

# Import target "FIRCodeGen" for configuration "Release"
set_property(TARGET FIRCodeGen APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(FIRCodeGen PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELEASE "CXX"
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/lib/libFIRCodeGen.a"
  )

list(APPEND _IMPORT_CHECK_TARGETS FIRCodeGen )
list(APPEND _IMPORT_CHECK_FILES_FOR_FIRCodeGen "${_IMPORT_PREFIX}/lib/libFIRCodeGen.a" )

# Import target "FIRDialect" for configuration "Release"
set_property(TARGET FIRDialect APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(FIRDialect PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELEASE "CXX"
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/lib/libFIRDialect.a"
  )

list(APPEND _IMPORT_CHECK_TARGETS FIRDialect )
list(APPEND _IMPORT_CHECK_FILES_FOR_FIRDialect "${_IMPORT_PREFIX}/lib/libFIRDialect.a" )

# Import target "FIRSupport" for configuration "Release"
set_property(TARGET FIRSupport APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(FIRSupport PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELEASE "CXX"
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/lib/libFIRSupport.a"
  )

list(APPEND _IMPORT_CHECK_TARGETS FIRSupport )
list(APPEND _IMPORT_CHECK_FILES_FOR_FIRSupport "${_IMPORT_PREFIX}/lib/libFIRSupport.a" )

# Import target "FIRTransforms" for configuration "Release"
set_property(TARGET FIRTransforms APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(FIRTransforms PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELEASE "CXX"
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/lib/libFIRTransforms.a"
  )

list(APPEND _IMPORT_CHECK_TARGETS FIRTransforms )
list(APPEND _IMPORT_CHECK_FILES_FOR_FIRTransforms "${_IMPORT_PREFIX}/lib/libFIRTransforms.a" )

# Import target "bbc" for configuration "Release"
set_property(TARGET bbc APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(bbc PROPERTIES
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/bin/bbc"
  )

list(APPEND _IMPORT_CHECK_TARGETS bbc )
list(APPEND _IMPORT_CHECK_FILES_FOR_bbc "${_IMPORT_PREFIX}/bin/bbc" )

# Import target "flang-new" for configuration "Release"
set_property(TARGET flang-new APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(flang-new PROPERTIES
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/bin/flang-new"
  )

list(APPEND _IMPORT_CHECK_TARGETS flang-new )
list(APPEND _IMPORT_CHECK_FILES_FOR_flang-new "${_IMPORT_PREFIX}/bin/flang-new" )

# Import target "tco" for configuration "Release"
set_property(TARGET tco APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(tco PROPERTIES
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/bin/tco"
  )

list(APPEND _IMPORT_CHECK_TARGETS tco )
list(APPEND _IMPORT_CHECK_FILES_FOR_tco "${_IMPORT_PREFIX}/bin/tco" )

# Import target "f18-parse-demo" for configuration "Release"
set_property(TARGET f18-parse-demo APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(f18-parse-demo PROPERTIES
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/bin/f18-parse-demo"
  )

list(APPEND _IMPORT_CHECK_TARGETS f18-parse-demo )
list(APPEND _IMPORT_CHECK_FILES_FOR_f18-parse-demo "${_IMPORT_PREFIX}/bin/f18-parse-demo" )

# Import target "fir-opt" for configuration "Release"
set_property(TARGET fir-opt APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(fir-opt PROPERTIES
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/bin/fir-opt"
  )

list(APPEND _IMPORT_CHECK_TARGETS fir-opt )
list(APPEND _IMPORT_CHECK_FILES_FOR_fir-opt "${_IMPORT_PREFIX}/bin/fir-opt" )

# Import target "FortranRuntime" for configuration "Release"
set_property(TARGET FortranRuntime APPEND PROPERTY IMPORTED_CONFIGURATIONS RELEASE)
set_target_properties(FortranRuntime PROPERTIES
  IMPORTED_LINK_INTERFACE_LANGUAGES_RELEASE "C;CXX"
  IMPORTED_LOCATION_RELEASE "${_IMPORT_PREFIX}/lib/libFortranRuntime.a"
  )

list(APPEND _IMPORT_CHECK_TARGETS FortranRuntime )
list(APPEND _IMPORT_CHECK_FILES_FOR_FortranRuntime "${_IMPORT_PREFIX}/lib/libFortranRuntime.a" )

# Commands beyond this point should not need to know the version.
set(CMAKE_IMPORT_FILE_VERSION)
